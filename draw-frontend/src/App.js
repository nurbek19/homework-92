import React, {Component} from 'react';

class App extends Component {
    state = {
        mouseDown: false,
        pixelsArray: []
    };

    drawImage = (x, y) => {
        this.context = this.canvas.getContext('2d');
        this.imageData = this.context.createImageData(1, 1);
        this.d = this.imageData.data;

        this.d[0] = 0;
        this.d[1] = 0;
        this.d[2] = 0;
        this.d[3] = 255;

        this.context.putImageData(this.imageData, x, y);
    };

    componentDidMount() {
        this.websocket = new WebSocket('ws://localhost:8000/draw');

        this.websocket.onmessage = (message) => {
            const decodedMessage = JSON.parse(message.data);

            switch (decodedMessage.type) {
                case 'NEW_MESSAGE':

                    for (let i = 0; i < decodedMessage.pixels.length; i++) {
                        this.drawImage(decodedMessage.pixels[i].x, decodedMessage.pixels[i].y);
                    }

                    break;
            }
        };
    }

    canvasMouseMoveHandler = event => {
        if (this.state.mouseDown) {
            event.persist();
            this.setState(prevState => {
                return {
                    pixelsArray: [...prevState.pixelsArray, {
                        x: event.clientX,
                        y: event.clientY
                    }]
                };
            });

            this.drawImage(event.clientX, event.clientY);
        }
    };

    mouseDownHandler = () => {
        this.setState({mouseDown: true});
    };

    mouseUpHandler = () => {
        const image = JSON.stringify({
            type: 'CREATE_IMAGE',
            pixels: this.state.pixelsArray
        });

        this.websocket.send(image);

        this.setState({mouseDown: false, pixelsArray: []});
    };

    render() {
        return (
            <canvas
                ref={elem => this.canvas = elem}
                style={{border: '5px solid black'}}
                width={800}
                height={600}
                onMouseDown={this.mouseDownHandler}
                onMouseUp={this.mouseUpHandler}
                onMouseMove={this.canvasMouseMoveHandler}
            />
        );
    }
}

export default App;
